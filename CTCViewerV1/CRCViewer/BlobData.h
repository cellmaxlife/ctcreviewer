#pragma once
#include <vector>

using namespace std;

class CBlobData
{
public:
	CBlobData(int width, int height);
	virtual ~CBlobData();
	int m_Width;
	int m_Height;
	RECT m_Rect;
	int m_PixelCount;
	int m_Threshold;
	int m_MaxIntensity;
	int m_MaxIntenX;
	int m_MaxIntenY;
	int m_AverageIntensity;
	vector<unsigned int> *m_Boundary;
	unsigned char *m_Map;
	void CopyData(CBlobData *blob);
	int m_IntensityRange;
};

typedef struct peakinfo_tag
{
public:
	int maxIntensity;
	int maxIntenX;
	int maxIntenY;
	int threshold;
	int pixelcount;
} PEAKINFO;
