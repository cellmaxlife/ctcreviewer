#pragma once

#include "resource.h"

// AddOneCTCDlg 對話方塊

class AddOneCTCDlg : public CDialog
{
	DECLARE_DYNAMIC(AddOneCTCDlg)

public:
	AddOneCTCDlg(CString message, CWnd* pParent = NULL);   // 標準建構函式
	virtual ~AddOneCTCDlg();

// 對話方塊資料
	enum { IDD = IDD_ADDONECTC };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支援

	DECLARE_MESSAGE_MAP()
public:
	CString m_Message;
};
