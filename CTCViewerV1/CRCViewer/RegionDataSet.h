#include <vector>

using namespace std;

typedef struct intensity_pixel_set_tag
{
	unsigned short intensity;
	vector<int> *pixelList;
	intensity_pixel_set_tag(unsigned short inten, int pixel)
	{
		intensity = inten;
		pixelList = new vector<int>();
		pixelList->push_back(pixel);
	}
	~intensity_pixel_set_tag()
	{
		pixelList->clear();
		delete pixelList;
	}
} INTENSITY_PIXEL_SET;

typedef struct regiondataset_tag
{
	int reference;
	int position;
	unsigned short intensity;
	vector<int> *pixels;
	struct regiondataset_tag *child;
	regiondataset_tag(int ref, int pos, unsigned short inten)
	{
		reference = ref;
		position = pos;
		intensity = inten;
		pixels = new vector<int>();
		pixels->push_back(pos);
		child = NULL;
	}
	regiondataset_tag(struct regiondataset_tag *region, unsigned short inten)
	{
		reference = region->reference;
		position = region->position;
		intensity = inten;
		pixels = new vector<int>();
		for (int i = 0; i < (int)region->pixels->size(); i++)
			pixels->push_back((*region->pixels)[i]);
		child = NULL;
	}
	~regiondataset_tag()
	{
		child = NULL;
		pixels->clear();
		delete pixels;
	}
} REGIONDATASET;

typedef struct blobcandidate_tag
{
	int intensitylevels;
	unsigned short threshold;
	vector<int> *pixels;
	blobcandidate_tag(int levels, unsigned short intensity)
	{
		intensitylevels = levels;
		threshold = intensity;
		pixels = new vector<int>();
	}
	~blobcandidate_tag()
	{
		pixels->clear();
		delete pixels;
	}
} BLOBCANDIDATE;

typedef struct region_sample_tag
{
	int sampleIndex;
	double value;
} REGIONSAMPLE;