
// CTCViewerDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "Log.h"
#include "SingleChannelTIFFData.h"
#include "RGNData.h"
#include <vector>
#include "afxcmn.h"
#include "HitFindingOperation.h"
#include "ColorType.h"
#include "CTCParams.h"

using namespace std;

// CCTCViewerDlg dialog
class CCTCViewerDlg : public CDialogEx
{
// Construction
public:
	CCTCViewerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CTCVIEWER_DIALOG };

	protected:	
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	CLog m_Log;
	CSingleChannelTIFFData *m_RedTIFFData;
	CSingleChannelTIFFData *m_GreenTIFFData;
	CSingleChannelTIFFData *m_BlueTIFFData;
	CImage m_Image;
	CImage m_Pad;
	CImage m_ColorImage[3];
	int m_ImageWidth;
	int m_ImageHeight;
	void CopyToRGBImage(unsigned short *pBlueBuffer, unsigned short *pGreenBuffer, unsigned short *pRedBuffer, CImage *pOutImage, CRGNData *hitData);
	void CopyToRGBPad(unsigned int color);
	vector<CRGNData*> m_RGNDataArray;
	void FreeRGNData(vector<CRGNData *> *rgnList);
	void UpdateColorCodeSelection(void);
	BOOL DisplayROI(int index);
	void CountColorCode(void);
	BOOL SaveRGNFile();
	int m_DisplayedRgnIndex;
	void ResetSlider();
	BYTE GetContrastEnhancedByte(unsigned short value, int maxValue, PIXEL_COLOR_TYPE color);
	bool GetNextIndex();
	bool GetPrevIndex();
	int m_BoxWidth;
	int m_BoxHeight;
	void ResetBoxPos();
	void DisplayBoxDescription(void);
	void GetAverageIntensity(int *x0, int *y0, int width, int height, int *red, int *green, int *blue, int *redmax, int *greenmax, int *bluemax);
	void CalculateAverageIntensity(unsigned short *image, int width1, int x0, int y0, int width, int height, 
		PIXEL_COLOR_TYPE color, int *average, int *max);
	BOOL SaveBMPImage(CString filename, CImage *image);
	int m_RegionX0;
	int m_RegionY0;
	int m_RegionWidth;
	int m_RegionHeight;
	unsigned short *m_RedRegionImage;
	unsigned short *m_GreenRegionImage;
	unsigned short *m_BlueRegionImage;
	void LoadRegionImageFile(int index, int mag);
	void SaveTempRegionFile(void);
	int getCellSize(BYTE *image, int width, int height);
	void Get8BitImageData(CImage *rgb, CImage *red, CImage *green, CImage *blue);
	BOOL Save4BMPFiles(CString filename);
	CCTCParams m_CTCParams;
	void AutoScreen(int index);
	void FreeRegionImageData();
	CHitFindingOperation m_HitFinder;
	BOOL LoadRegionData(CString pathname);
	vector<CString> m_Comments;
	void ReadComments(CString filename);
	void SaveComments(CString filename);
	CString m_CommentFileName;
	void DisplayComment(int regionIndex);
	void SaveWBCGreenAvg(CString filename);
	void LoadWBCGreenAvg(CString filename);

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	bool ReadRgnfile();
	CStatic m_ImageDisplay;
	CString m_RGNFilename;
	CString m_TIFFFilename;
	CString m_FullPathTIFFFilename;
	CString m_SampleName;
	CString m_Status;
	CButton m_Blue;
	CButton m_Green;
	CButton m_Red;
	void ClickColor();
	afx_msg void OnBnClickedBlue();
	afx_msg void OnBnClickedRed();
	afx_msg void OnBnClickedGreen();
	afx_msg void OnBnClickedNext();
	afx_msg void OnBnClickedPrev();
	int m_CTCCount;
	CStatic m_ColorPadDisplay;
	afx_msg void OnBnClickedSavergn();
	CButton m_CTCRadio;
	CButton m_NonCTCRadio;
	CButton m_ConfirmedCTCRadio;
	afx_msg void OnBnClickedCtcradio();
	afx_msg void OnBnClickedNonctcradio();
	CStatic m_CLightBlue;
	CStatic m_CPink;
	CStatic m_CRed;
	afx_msg void OnBnClickedConfirmedctc();
	int m_ConfirmedCTCNum;
	int m_ConfirmedNonCTCNum;
	int m_CTCNum;
	CEdit m_CTCIndex;
	afx_msg void OnBnClickedSelect();
	CSliderCtrl m_BlueSlider;
	CSliderCtrl m_GreenSlider;
	CSliderCtrl m_RedSlider;
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	BOOL PreTranslateMessage(MSG* pMsg);
	int m_BlueIntensity;
	int m_GreenIntensity;
	int m_RedIntensity;
	CButton m_UseKeyStroke;
	RECT m_ROIRect;
	void DrawROIWindow(CDC* dc, RECT rc);
	void getImagePos(RECT roi, int *column, int *row);
	CString m_Description;
	afx_msg void OnBnClickedSaveimage();
	CButton m_UseIntensityBox;
	afx_msg void OnBnClickedRedreset();
	afx_msg void OnBnClickedGreenreset();
	afx_msg void OnBnClickedBluereset();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	CSliderCtrl m_Mag1;
	CString m_GreenFilename;
	CString m_BlueFilename;
	afx_msg void OnBnClickedRedfile();
	bool ReadGreenfile();
	bool ReadBluefile();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedResetcontrast();
	CListCtrl m_CellScoreList;
	afx_msg void OnBnClickedResetc2();
	afx_msg void OnBnClickedUsebox();
	afx_msg void OnBnClickedReload();
	afx_msg void OnBnClickedSavedata();
	afx_msg LRESULT OnMyMessage(WPARAM wparam, LPARAM lparam);
	afx_msg LRESULT OnMyMessage2(WPARAM wparam, LPARAM lparam);
	CButton m_LoadOneChannel;
	CString m_Comment;
	int m_RedBoundaryThreshold;
	CButton m_ShowBoundary;
	CButton m_RankPossibleOnly;
	CButton m_RankConfirmedOnly;
	CButton m_RankAllRegions;
	CButton m_TwoTypeCTCs;
	CButton m_NonCTCOnly;
	CString Leica_Red_Prefix;
	CString Leica_Green_Prefix;
	CString Leica_Blue_Prefix;
	CString Zeiss_Red_Postfix;
	CString Zeiss_Green_Postfix;
	CString Zeiss_Blue_Postfix;
	void LoadDefaultSettings();
	CString GetCommentContents(int index);
	bool m_ZeissData;
	afx_msg void OnBnClickedOpenrgn();
	afx_msg void OnBnClickedAddone();
	void EnableButtons(BOOL enable);
	afx_msg void OnBnClickedForctconly();
	afx_msg void OnBnClickedForctc2only();
	afx_msg void OnBnClickedTwotypectcs();
	afx_msg void OnBnClickedNonctconly();
	afx_msg void OnBnClickedForallregions();
	void StartFromHeadOfList();
	CButton m_ResetZoomOut;
	afx_msg void OnBnClickedZoomin();
	CString m_ReviewerName;
	CButton m_ReviewAnswer[REVIEW_ANSWERS];
	void ResetReviewAnswer();
	void SetReviewerAnswer(bool *answers);
	void UpDateReviewAnswer(int AnswerIndex);
	afx_msg void OnBnClickedBasic1();
	afx_msg void OnBnClickedBasic2();
	afx_msg void OnBnClickedCato1a();
	afx_msg void OnBnClickedCato1b();
	afx_msg void OnBnClickedCato2a();
	afx_msg void OnBnClickedCato2b();
	afx_msg void OnBnClickedCato3a();
	afx_msg void OnBnClickedCato3b();
	afx_msg void OnBnClickedCato3c();
	void LoadCheckListData(CString filename);
	void SaveCheckListData(CString filename);
	CString m_ReviewerNameInRGNFile;
	void ChangeRadioButtonSelection(unsigned int colorCode);
};



