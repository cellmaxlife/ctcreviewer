#include "stdafx.h"
#include "HitFindingOperation.h"
#include <string.h>
#include "CTCParams.h"
#include "VersionNumber.h"
#include <math.h>

#define DEFAULT_MIN_BLOBPIXELS	20
#define MAX_NUM_BLOB_PER_FRAME	10
#define SAMPLEFRAMEWIDTH 100
#define SAMPLEFRAMEHEIGHT 100
#define PI 3.14159265

CHitFindingOperation::CHitFindingOperation()
{
	m_MinBlobPixelCount = 100;
	m_MaxBlobPixelCount = 900;
}

CHitFindingOperation::~CHitFindingOperation()
{
}

void CHitFindingOperation::ProcessOneROI(CCTCParams *params, CRGNData *hitData, vector<CRGNData *> *hitsFound, bool useAutoThreshold)
{
	hitData->FreeBlobList(RED_COLOR);
	hitData->FreeBlobList(GREEN_COLOR);
	hitData->FreeBlobList(BLUE_COLOR);
	hitData->FreeBlobList(RB_COLORS);
	hitData->FreeBlobList(GB_COLORS);
	
	vector<CColorStain *> cellCandidates;
	if (useAutoThreshold)
		GetBlobs(params, hitData, &cellCandidates);
	else
		GetBlobsWithFixedThreshold(params, hitData, &cellCandidates);

	if (cellCandidates.size() == 0)
	{
		return;
	}

	int width = hitData->GetWidth();
	int height = hitData->GetHeight();
	unsigned char *greenRingMap = new unsigned char[sizeof(unsigned char) * width * height];
	unsigned char *temp = new unsigned char[width * height];

	for (int m = 0; m < (int)cellCandidates.size(); m++)
	{
		int redPixelCounts = 0;
		int greenPixelCounts = 0;
		int bluePixelCounts = 0;
		int blueRedIntersectPixelCounts = 0;

		CRGNData *ptr = cellCandidates[m]->m_RedRgnPtr;
		cellCandidates[m]->m_RedRgnPtr = NULL;
		CBlobData *rptr = cellCandidates[m]->m_RedBlob;
		cellCandidates[m]->m_RedBlob = NULL;
		ptr->m_WBCGreenAvg = params->m_WBCGreenAvg - hitData->GetCPI(GREEN_COLOR);

		unsigned short *redImg = hitData->GetImage(RED_COLOR);
		unsigned short *greenImg = hitData->GetImage(GREEN_COLOR);
		unsigned short *blueImg = hitData->GetImage(BLUE_COLOR);

		ptr->SetBoundingBox(rptr->m_Rect.left, rptr->m_Rect.top, rptr->m_Rect.right, rptr->m_Rect.bottom);
		ptr->m_AspectRatio = GetAspectRatio(rptr, ptr);
		hitData->m_AspectRatio = ptr->m_AspectRatio;
		hitData->m_LongAxisLength = ptr->m_LongAxisLength;

		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				if (rptr->m_Map[width * i + j] == (unsigned char)255) 
				{
					int redValue = (int)redImg[width * i + j];
					ptr->m_RedAverage += redValue;
					if (redValue > ptr->m_RedMax)
						ptr->m_RedMax = redValue;
					redPixelCounts++;
				}
			}
		}

		if (redPixelCounts > 0)
			ptr->m_RedAverage /= redPixelCounts;
		else
			ptr->m_RedAverage = 0;

		if (cellCandidates[m]->m_GreenBlob != NULL)
		{
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					if ((cellCandidates[m]->m_GreenBlob->m_Map[width * i + j] == (unsigned char)255) &&
						(NeighborhoodON(rptr, i, j)))
					{
						int greenValue = (int)greenImg[width * i + j];
						ptr->m_GreenAverage += greenValue;
						if (greenValue > ptr->m_GreenMax)
							ptr->m_GreenMax = greenValue;
						greenPixelCounts++;
					}
				}
			}
		}

		for (int k = 0; k < (int)cellCandidates[m]->m_BlueBlobs.size(); k++)
		{
			CBlobData *blueBlob = cellCandidates[m]->m_BlueBlobs[k];
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					if ((blueBlob->m_Map[width * i + j] == (unsigned char)255) &&
						(NeighborhoodON(rptr, i, j)))
					{
						int blueValue = (int)blueImg[width * i + j];
						ptr->m_BlueAverage += blueValue;
						if (blueValue > ptr->m_BlueMax)
							ptr->m_BlueMax = blueValue;
						blueRedIntersectPixelCounts++;
					}
				}
			}
			bluePixelCounts += blueBlob->m_PixelCount;
		}
		if (blueRedIntersectPixelCounts > 0)
			ptr->m_BlueAverage /= blueRedIntersectPixelCounts;
		else
			ptr->m_BlueAverage = 0;

		ptr->SetPixels(RB_COLORS, blueRedIntersectPixelCounts);
		ptr->SetPixels(RED_COLOR, redPixelCounts);
		ptr->SetPixels(GREEN_COLOR, greenPixelCounts);
		ptr->SetPixels(BLUE_COLOR, bluePixelCounts);

		if (greenPixelCounts == 0)
			ptr->m_GreenAverage = 0;
		else
			ptr->m_GreenAverage /= greenPixelCounts;

		if (ptr->m_RedAverage > 0)
			ptr->m_RedValue = ptr->m_RedAverage - hitData->GetCPI(RED_COLOR);
		else
			ptr->m_RedValue = 0;

		ptr->m_RedScore = 0;
		if (ptr->m_RedValue >= params->m_CTCParams[PARAM_AVERAGECK15])
			ptr->m_RedScore = 15;
		else if (ptr->m_RedValue >= params->m_CTCParams[PARAM_AVERAGECK10])
			ptr->m_RedScore = 10;
		else if (ptr->m_RedValue >= params->m_CTCParams[PARAM_AVERAGECK5])
			ptr->m_RedScore = 5;
		else if (ptr->m_RedValue < params->m_CTCParams[PARAM_AVERAGECK_5])
			ptr->m_RedScore = -5;

		ptr->m_CellSizeScore = 0;
		ptr->m_CellSizeValue = (float) (0.648 * ptr->m_LongAxisLength);
		if (ptr->m_CellSizeValue > params->m_CTCParams[PARAM_CELLSIZE15])
			ptr->m_CellSizeScore = 15;
		else if (ptr->m_CellSizeValue > params->m_CTCParams[PARAM_CELLSIZE10])
			ptr->m_CellSizeScore = 10;
		else if (ptr->m_CellSizeValue > params->m_CTCParams[PARAM_CELLSIZE5])
			ptr->m_CellSizeScore = 5;
		else if (ptr->m_CellSizeValue < params->m_CTCParams[PARAM_CELLSIZE_5])
			ptr->m_CellSizeScore = -5;
		else if (ptr->m_CellSizeValue < params->m_CTCParams[PARAM_CELLSIZE_10])
			ptr->m_CellSizeScore = -10;

		ptr->m_NCRatio = 0.0F;
		ptr->m_NCScore = 0;
		if (redPixelCounts > 0)
		{
			ptr->m_NCRatio = (float)(100.0 * ((double)ptr->GetPixels(RB_COLORS)) / ((double)ptr->GetPixels(RED_COLOR)));
			if (ptr->m_NCRatio >= params->m_CTCParams[PARAM_NCRATIO0])
				ptr->m_NCScore = -10;
			else if (ptr->m_NCRatio >= (params->m_CTCParams[PARAM_NCRATIO0] - 1))
				ptr->m_NCScore = 0;
			else if (ptr->m_NCRatio > params->m_CTCParams[PARAM_NCRATIO15])
				ptr->m_NCScore = 15;
			else if (ptr->m_NCRatio > params->m_CTCParams[PARAM_NCRATIO10])
				ptr->m_NCScore = 10;
			else if (ptr->m_NCRatio > params->m_CTCParams[PARAM_NCRATIO5])
				ptr->m_NCScore = 5;
		}

		ptr->m_GreenValue = ptr->m_GreenAverage - hitData->GetCPI(GREEN_COLOR);
		ptr->m_GreenAboveWBC = ptr->m_GreenValue - ptr->m_WBCGreenAvg;

		ptr->m_GreenScore = 0;
		if (ptr->m_GreenAboveWBC >= params->m_CTCParams[PARAM_CD45AVERAGE_25])
			ptr->m_GreenScore = -25;
		else if (ptr->m_GreenAboveWBC >= params->m_CTCParams[PARAM_CD45AVERAGE_20])
			ptr->m_GreenScore = -20;
		else if (ptr->m_GreenAboveWBC >= params->m_CTCParams[PARAM_CD45AVERAGE_15])
			ptr->m_GreenScore = -15;
		else if (ptr->m_GreenAboveWBC >= params->m_CTCParams[PARAM_CD45AVERAGE_10])
			ptr->m_GreenScore = -10;
		else if (ptr->m_GreenAboveWBC >= params->m_CTCParams[PARAM_CD45AVERAGE_5])
			ptr->m_GreenScore = -5;
		else if (ptr->m_GreenAboveWBC < params->m_CTCParams[PARAM_CD45AVERAGE5])
			ptr->m_GreenScore = 5;
		else if (ptr->m_GreenAboveWBC < params->m_CTCParams[PARAM_CD45AVERAGE10])
			ptr->m_GreenScore = 10;

		ptr->GetBlobData(RED_COLOR)->push_back(rptr);

		for (int k = 0; k < (int)cellCandidates[m]->m_BlueBlobs.size(); k++)
		{
			GetBlobBoundary(cellCandidates[m]->m_BlueBlobs[k]);
			ptr->GetBlobData(BLUE_COLOR)->push_back(cellCandidates[m]->m_BlueBlobs[k]);
			cellCandidates[m]->m_BlueBlobs[k] = NULL;
		}
		FreeBlobList(&cellCandidates[m]->m_BlueBlobs);

		int outerRingIntensityAverage = 0;
		int innerRingIntensityAverage = 0;
		if (cellCandidates[m]->m_GreenBlob != NULL)
		{
			CBlobData *greenBlob = cellCandidates[m]->m_GreenBlob;
			GetBlobBoundary(greenBlob);
			ptr->GetBlobData(GREEN_COLOR)->push_back(cellCandidates[m]->m_GreenBlob);
			cellCandidates[m]->m_GreenBlob = NULL;
			memcpy(greenRingMap, greenBlob->m_Map, sizeof(unsigned char) * width * height);
			for (int i = 0; i < params->m_CTCParams[(int)PARAM_RINGPIXELS]; i++)
			{
				ErosionOperation(greenRingMap, temp, width, height);
				memcpy(greenRingMap, temp, sizeof(unsigned char) * width * height);
			}
			CBlobData *blobPtr = new CBlobData(width, height);
			blobPtr->m_Width = rptr->m_Width;
			blobPtr->m_Height = rptr->m_Height;
			GetBlobDataFromMap(blobPtr, greenRingMap, 255, greenImg, 0);
			GetBlobBoundary(blobPtr);
			ptr->GetBlobData(GB_COLORS)->push_back(blobPtr);
			unsigned char *ptrGreenMap = greenBlob->m_Map;
			unsigned char *ptrGreenRingMap = greenRingMap;
			unsigned short *ptrGreenImage = greenImg;
			unsigned char label = (unsigned char)255;
			int greenValueSum = 0;
			int greenValueCount = 0;
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++, ptrGreenRingMap++, ptrGreenImage++)
				{
					if (*ptrGreenRingMap == label)
					{
						greenValueSum += (int)(*ptrGreenImage);
						greenValueCount++;
					}
				}
			}
			if (greenValueCount > 0)
			{ 
				int innerRingAverage = greenValueSum / greenValueCount;
				ptrGreenMap = greenBlob->m_Map;
				ptrGreenRingMap = greenRingMap;
				ptrGreenImage = greenImg;
				greenValueSum = 0;
				greenValueCount = 0;
				int sum = 0;
				int sumCount = 0;
				for (int i = 0; i < height; i++)
				{
					for (int j = 0; j < width; j++, ptrGreenMap++, ptrGreenRingMap++, ptrGreenImage++)
					{
						if ((*ptrGreenMap == label) && (*ptrGreenRingMap == (unsigned char)0))
						{
							int greenValue = (int)(*ptrGreenImage);
							if (greenValue > innerRingAverage)
							{
								sum += greenValue;
								sumCount++;
							}
						}
						if (*ptrGreenRingMap == label)
						{
							int greenValue = (int)(*ptrGreenImage);
							if (greenValue > innerRingAverage)
							{
								greenValueSum += greenValue;
								greenValueCount++;
							}
						}
					}
				}
				if ((sumCount > 0) && (greenValueCount > 0))
				{
					outerRingIntensityAverage = sum / sumCount;
					innerRingIntensityAverage = greenValueSum / greenValueCount;
					if (innerRingIntensityAverage > 0)
						ptr->m_GreenRingValue = (float)(100.0 * ((double)(outerRingIntensityAverage - innerRingIntensityAverage)) / ((double)innerRingIntensityAverage));
					else
						ptr->m_GreenRingValue = 0;
				}
			}
		}
		
		if (ptr->m_GreenRingValue > params->m_CTCParams[PARAM_CD45RING25])
			ptr->m_GreenRingScore = -25;
		else if (ptr->m_GreenRingValue > params->m_CTCParams[PARAM_CD45RING20])
			ptr->m_GreenRingScore = -20;
		else if (ptr->m_GreenRingValue > params->m_CTCParams[PARAM_CD45RING15])
			ptr->m_GreenRingScore = -15;
		else if (ptr->m_GreenRingValue > params->m_CTCParams[PARAM_CD45RING10])
			ptr->m_GreenRingScore = -10;
		else if (ptr->m_GreenRingValue > params->m_CTCParams[PARAM_CD45RING5])
			ptr->m_GreenRingScore = -5;

		int score = 50 + ptr->m_RedScore + ptr->m_CellSizeScore + ptr->m_NCScore + ptr->m_GreenScore + ptr->m_GreenRingScore;
		ptr->SetScore(score);
		hitData->SetScore(score);
		double RedArea = ptr->GetPixels(RED_COLOR);
		if (ptr->m_LongAxisLength > 0)
		{
			ptr->m_Roundness = (int)(400.0 * RedArea / (PI * ptr->m_LongAxisLength * ptr->m_LongAxisLength));
		}
		else
			ptr->m_Roundness = 0;
		hitData->m_Roundness = ptr->m_Roundness;
		ptr->SetColorCode(CTC);
		ptr->InitReviewAnswer(CTC);
		hitsFound->push_back(ptr);
	}

	delete[] greenRingMap;
	delete[] temp;

	for (int i = 0; i < (int)cellCandidates.size(); i++)
	{
		delete cellCandidates[i];
	}
	cellCandidates.clear();
}

void CHitFindingOperation::GetMaxBlobFromMap(CBlobData *blob)
{
	int width = blob->m_Width;
	int height = blob->m_Height;
	unsigned short *image = new unsigned short[width * height];
	memset(image, 0, sizeof(unsigned short) * width * height);
	unsigned short *map = new unsigned short[width * height];
	memset(map, MAXUINT16, sizeof(unsigned short) * width * height);
	unsigned short *imgPtr = image;
	unsigned short *mapPtr = map;
	unsigned char *blobMapPtr = blob->m_Map;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, mapPtr++, imgPtr++, blobMapPtr++)
		{
			if (*blobMapPtr == (unsigned char)255)
			{
				*mapPtr = (unsigned short)0;
				*imgPtr = (unsigned short)255;
			}
		}
	}
	unsigned short lastLabel = 0;
	m_Grouper.LabelConnectedComponents(image, width, height, 255, map, &lastLabel);
	int numLabels = (int)lastLabel;
	int *pixelCounts = new int[numLabels];
	memset(pixelCounts, 0, sizeof(int) * numLabels);
	mapPtr = map;
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++)
		{
			if ((*mapPtr > 0) && (*mapPtr <= numLabels))
			{
				pixelCounts[(int)*mapPtr - 1]++;
			}
		}
	}
	int maxCount = 0;
	int maxIndex = -1;

	for (int i = 0; i < numLabels; i++)
	{
		if (pixelCounts[i] > maxCount)
		{
			maxCount = pixelCounts[i];
			maxIndex = i;
		}
	}
	int maxIndex1 = maxIndex;
	while (maxCount > m_MaxBlobPixelCount)
	{
		pixelCounts[maxIndex] = 0;
		maxCount = 0;
		maxIndex = -1;

		for (int i = 0; i < numLabels; i++)
		{
			if (pixelCounts[i] > maxCount)
			{
				maxCount = pixelCounts[i];
				maxIndex = i;
			}
		}
	}
	if ((maxIndex == -1) && (maxIndex1 > -1))
		maxIndex = maxIndex1;
	delete[] pixelCounts; 
	mapPtr = map;
	blobMapPtr = blob->m_Map;
	maxIndex++; 
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++, blobMapPtr++)
		{
			if (*mapPtr == maxIndex)
				*blobMapPtr = (unsigned char)255;
			else
				*blobMapPtr = (unsigned char)0;
		}
	}
	delete[] image;
	delete[] map;
}

void CHitFindingOperation::FreeBlobList(vector<CBlobData *> *blobList)
{
	for (int i = 0; i < (int)blobList->size(); i++)
	{
		if ((*blobList)[i] != NULL)
			delete (*blobList)[i];
	}
	blobList->clear();
}

void CHitFindingOperation::DumpRegionDataToCSVFile(CString filename, vector<CRGNData *> *list, bool isCellMapPlus)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		CString textline;
		if (isCellMapPlus)
			textline.Format(_T("HitFinding Results of CellMapPlus %s\n"), CELLMAPPLUS_VERSION);
		else
			textline.Format(_T("HitFinding Results of CRCViewer %s\n"), CELLMAPPLUS_VERSION);
		theFile.WriteString(textline);
		textline.Format(_T("Total Number of Hits = %u\n"), list->size());
		theFile.WriteString(textline);
		textline.Format(_T("RgnIdx,Color,Score,X0,Y0,Left,Top,Right,Bottom,ARatio,RAvg,GAvg,BAvg,RPxls,GPxls,BPxls,RBPxls,Roundness,LongAxisLength\n"));
		theFile.WriteString(textline);
		for (int i = 0; i < (int)list->size(); i++)
		{
			CRGNData *ptr = (*list)[i];
			int x0, y0;
			ptr->GetPosition(&x0, &y0);
			int left, top, right, bottom;
			ptr->GetBoundingBox(&left, &top, &right, &bottom);
			textline.Format(_T("%d,%u,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n"),
				i + 1, ptr->GetColorCode(), ptr->GetScore(), x0, y0, left, top, right, bottom, ptr->m_AspectRatio,
				ptr->m_RedValue, ptr->m_GreenValue, ptr->m_BlueAverage-ptr->GetCPI(BLUE_COLOR), ptr->GetPixels(RED_COLOR), ptr->GetPixels(GREEN_COLOR), ptr->GetPixels(BLUE_COLOR),
				ptr->GetPixels(RB_COLORS), ptr->m_Roundness, ptr->m_LongAxisLength);
			theFile.WriteString(textline);
		}
		theFile.Close();
	}
}

void CHitFindingOperation::GetBlueBlobs(CCTCParams *params, CRGNData *hitData, CBlobData *redBlob)
{
	FreeBlobList(hitData->GetBlobData(BLUE_COLOR));
	int width = redBlob->m_Width;
	int height = redBlob->m_Height;
	unsigned char *newMap1 = new unsigned char[width * height];
	DilationOperation(redBlob->m_Map, newMap1, width, height);
	unsigned short *image = hitData->GetImage(BLUE_COLOR);
	int maxIntensity = 0;
	int minIntensity = MAXUINT16;
	unsigned char *mapPtr = newMap1;
	unsigned short *imgPtr = image;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, mapPtr++, imgPtr++)
		{
			if (*mapPtr == (unsigned char)255)
			{
				int intensity = (int)*imgPtr;
				if (intensity > maxIntensity)
					maxIntensity = intensity;
				if (intensity < minIntensity)
					minIntensity = intensity;
			}
		}
	}
	int size = maxIntensity - minIntensity + 1;
	int *histogram = new int[size];
	memset(histogram, 0, sizeof(int) * size);
	unsigned short *map = new unsigned short[width * height];
	memset(map, MAXUINT16, sizeof(unsigned short) * width * height);
	mapPtr = newMap1;
	unsigned short *mapPtr1 = map;
	imgPtr = image;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, mapPtr++, imgPtr++, *mapPtr1++)
		{
			if (*mapPtr == (unsigned char)255)
			{
				int intensity = (int)*imgPtr;
				*mapPtr1 = (unsigned short)0;
				histogram[intensity - minIntensity]++;
			}
		}
	}
	delete[] newMap1;
	int threshold = minIntensity + params->m_CTCParams[(int)PARAM_REDTHRESHOLD_DROP];
	unsigned short lastLabel = 0;
	m_Grouper.LabelConnectedComponents(image, width, height, threshold, map, &lastLabel);
	GetFrameMap(map, width, height, (int)lastLabel);
	unsigned short *frameMap = new unsigned short[width * height];
	int previous_count = 0;
	int count = 0;
	vector<PEAKINFO *> peaks;
	for (int i = (size - 1); i > 0; i--)
	{
		count += histogram[i];
		if ((count - previous_count) >= 10)
		{
			previous_count = count;
			memcpy(frameMap, map, sizeof(unsigned short) * width * height);
			lastLabel = 0;
			m_Grouper.LabelConnectedComponents(image, width, height, i+minIntensity, frameMap, &lastLabel);
			GetPeaks(image, frameMap, width, height, (int)lastLabel, DEFAULT_MIN_BLOBPIXELS, i+minIntensity, &peaks);
		}
	}
	delete[] histogram;
	vector<CBlobData *> blobs;
	for (int i = 0; i < (int)peaks.size(); i++)
	{
		memcpy(frameMap, map, sizeof(unsigned short) * width * height);
		CBlobData *blob = new CBlobData(width, height);
		int peakThreshold = 5 * peaks[i]->threshold / 8;
		GetBlobDataFromPeak(image, frameMap, width, height, peaks[i], blob, peakThreshold, &blobs, &peaks, i);
		if (blob->m_PixelCount >= DEFAULT_MIN_BLOBPIXELS) 
		{
			blobs.push_back(blob);
		}
		else
			delete blob;
	}
	delete[] map;
	delete[] frameMap;
	for (int i = 0; i < (int)peaks.size(); i++)
	{
		delete peaks[i];
		peaks[i] = NULL;
	}
	peaks.clear();
	for (int i = 0; i < (int)blobs.size(); i++)
	{
		if (blobs[i] != NULL)
		{
			hitData->GetBlobData(BLUE_COLOR)->push_back(blobs[i]);
			blobs[i] = NULL;
		}
	}
	blobs.clear();
}

void CHitFindingOperation::GetBlobs(CCTCParams *params, CRGNData *hitData, vector<CColorStain *> *stainList)
{
	m_MSERDetector.GetRedBlobs(hitData->GetImage(RED_COLOR), hitData->GetWidth(), hitData->GetHeight(), m_MinBlobPixelCount, m_MaxBlobPixelCount,
		params->m_CTCParams[(int)PARAM_SATURATED_REDCOUNT], 10, hitData->GetBlobData(RED_COLOR));
	if (hitData->GetBlobData(RED_COLOR)->size() > 0)
	{
		GetColorStains(params, hitData, stainList);
	}
}

void CHitFindingOperation::GetBlobsFromImageAndMap(unsigned short *image, unsigned short *map, int width, int height, int numLabels, 
	CRGNData *hitData, PIXEL_COLOR_TYPE color)
{
	int *pixelCounts = new int[numLabels];
	memset(pixelCounts, 0, sizeof(int) * numLabels);
	unsigned short *mapPtr = map;
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++)
		{
			if ((*mapPtr >(unsigned short)0) && (*mapPtr <= numLabels))
			{
				int index = ((int)*mapPtr) - 1;
				pixelCounts[index]++;
			}
		}
	}

	int *blobIndex = new int[numLabels];
	memset(blobIndex, 0, sizeof(int) * numLabels);
	int count = 0;
	for (int i = 0; i < numLabels; i++)
	{
		if (pixelCounts[i] > DEFAULT_MIN_BLOBPIXELS)
		{
			count++;
			blobIndex[i] = count;
			CBlobData *blob = new CBlobData(width, height);
			hitData->GetBlobData(color)->push_back(blob);
			blob->m_Rect.left = width;
			blob->m_Rect.right = 0;
			blob->m_Rect.top = height;
			blob->m_Rect.bottom = 0;
			blob->m_PixelCount = 0;
			blob->m_MaxIntensity = 0;
			blob->m_MaxIntenX = 0;
			blob->m_MaxIntenY = 0;
		}
	}
	mapPtr = map;
	unsigned short *imgPtr = image;
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++, imgPtr++)
		{
			if ((*mapPtr >(unsigned short)0) && (*mapPtr <= numLabels))
			{
				if (blobIndex[((int)*mapPtr) - 1] > 0)
				{
					CBlobData *blob = (*hitData->GetBlobData(color))[blobIndex[((int)*mapPtr) - 1] - 1];
					blob->m_Map[width * k + h] = (unsigned char)255;
					if (k < blob->m_Rect.top)
						blob->m_Rect.top = k;
					if (k > blob->m_Rect.bottom)
						blob->m_Rect.bottom = k;
					if (h < blob->m_Rect.left)
						blob->m_Rect.left = h;
					if (h > blob->m_Rect.right)
						blob->m_Rect.right = h;
					blob->m_PixelCount++;
					int intensity = (int)*imgPtr;
					if (intensity > blob->m_MaxIntensity)
					{
						blob->m_MaxIntensity = intensity;
						blob->m_MaxIntenX = h;
						blob->m_MaxIntenY = k;
					}
				}
			}
		}
	}
	delete[] pixelCounts;
	delete[] blobIndex;
}

void CHitFindingOperation::GetBlobsWithFixedThreshold(CCTCParams *params, CRGNData *hitData, vector<CColorStain *> *stainList)
{
	int width = hitData->GetWidth();
	int height = hitData->GetHeight();
	int redThreshold = hitData->GetThreshold(RED_COLOR);
	unsigned short lastLabel = 0;
	unsigned short *image = new unsigned short[width * height];
	unsigned short *map = new unsigned short[width * height];
	memset(map, 0, sizeof(unsigned short) * width * height);
	m_Grouper.LabelConnectedComponents(hitData->GetImage(RED_COLOR), width, height, redThreshold, map, &lastLabel);
	if (lastLabel > 0)
	{
		GetBlobsFromImageAndMap(hitData->GetImage(RED_COLOR), map, width, height, (int)lastLabel, hitData, RED_COLOR);
		if (hitData->GetBlobData(RED_COLOR)->size() > 0)
		{
			for (int i = 0; i < (int)hitData->GetBlobData(RED_COLOR)->size(); i++)
			{
				CBlobData *redBlob = (*hitData->GetBlobData(RED_COLOR))[i];
				int x0, y0;
				hitData->GetPosition(&x0, &y0);
				CRGNData *ptr = new CRGNData(x0, y0, hitData->GetWidth(), hitData->GetHeight());
				ptr->SetCPI(RED_COLOR, hitData->GetCPI(RED_COLOR));
				ptr->SetCPI(GREEN_COLOR, hitData->GetCPI(GREEN_COLOR));
				ptr->SetCPI(BLUE_COLOR, hitData->GetCPI(BLUE_COLOR));
				ptr->SetThreshold(RED_COLOR, hitData->GetThreshold(RED_COLOR));
				ptr->SetThreshold(GREEN_COLOR, hitData->GetThreshold(GREEN_COLOR));
				ptr->SetThreshold(BLUE_COLOR, hitData->GetThreshold(BLUE_COLOR));
				GetBlueBlobs(params, hitData, redBlob);
				vector<CBlobData *> blueBlobs;
				for (int j = 0; j < (int)(int)hitData->GetBlobData(BLUE_COLOR)->size(); j++)
				{
					CBlobData *blueBlob = (*hitData->GetBlobData(BLUE_COLOR))[j];
					int pixelCount = 0;
					GetIntersectPixels(redBlob, blueBlob, &pixelCount);
					if (pixelCount >= params->m_CTCParams[PARAM_INSIDEREDBLOB_BLUECOUNT])
					{
						blueBlobs.push_back(blueBlob);
						(*hitData->GetBlobData(BLUE_COLOR))[j] = NULL;
					}
				}
				if (blueBlobs.size() > 0)
				{
					unsigned char **ptrs = new unsigned char *[blueBlobs.size() + 1];
					ptrs[0] = redBlob->m_Map;
					for (int k = 0; k < (int)blueBlobs.size(); k++)
					{
						ptrs[k + 1] = blueBlobs[k]->m_Map;
					}
					memset(image, 0, sizeof(unsigned short) * width * height);
					memset(map, 0, sizeof(unsigned short) * width * height);
					for (int k = 0; k < height; k++)
					{
						for (int h = 0; h < width; h++)
						{
							bool pixelOn = false;
							for (int l = 0; l <= (int)blueBlobs.size(); l++)
							{
								if ((!pixelOn) && (*ptrs[l] == (unsigned char)255))
									pixelOn = true;
								ptrs[l]++;
							}
							if (pixelOn)
								image[width * k + h] = (unsigned short)255;
						}
					}
					delete[] ptrs;
					unsigned short lastLabel = 0;
					m_Grouper.LabelConnectedComponents(image, width, height, 255, map, &lastLabel);
					int numLabels = (int)lastLabel;
					int *pixelCounts = new int[numLabels];
					memset(pixelCounts, 0, sizeof(int) * numLabels);
					for (int k = 0; k < height; k++)
					{
						for (int h = 0; h < width; h++)
						{
							int mapValue = (int)map[width * k + h];
							if ((mapValue > 0) && (mapValue <= numLabels))
							{
								pixelCounts[mapValue - 1]++;
							}
						}
					}

					int maxCount = 0;
					int maxLabel = 0;
					for (int k = 0; k < numLabels; k++)
					{
						if (pixelCounts[k] > maxCount)
						{
							maxCount = pixelCounts[k];
							maxLabel = k;
						}
					}

					if (maxCount > 0)
					{
						maxLabel++;
						memset(redBlob->m_Map, 0, sizeof(unsigned char) * width * height);
						redBlob->m_PixelCount = 0;
						redBlob->m_Rect.left = width;
						redBlob->m_Rect.right = 0;
						redBlob->m_Rect.top = height;
						redBlob->m_Rect.bottom = 0;
						redBlob->m_MaxIntensity = 0;
						redBlob->m_MaxIntenX = 0;
						redBlob->m_MaxIntenY = 0;
						unsigned short *imgPtr = hitData->GetImage(RED_COLOR);
						unsigned short *mapPtr = map;
						for (int k = 0; k < height; k++)
						{
							for (int h = 0; h < width; h++, mapPtr++, imgPtr++)
							{
								if (maxLabel == (int)*mapPtr)
								{
									redBlob->m_PixelCount++;
									redBlob->m_Map[width * k + h] = (unsigned char)255;
									if (h < redBlob->m_Rect.left)
										redBlob->m_Rect.left = h;
									if (h > redBlob->m_Rect.right)
										redBlob->m_Rect.right = h;
									if (k < redBlob->m_Rect.top)
										redBlob->m_Rect.top = k;
									if (k > redBlob->m_Rect.bottom)
										redBlob->m_Rect.bottom = k;
									if ((int)*imgPtr > redBlob->m_MaxIntensity)
									{
										redBlob->m_MaxIntensity = (int)*imgPtr;
										redBlob->m_MaxIntenX = h;
										redBlob->m_MaxIntenY = k;
									}
								}
							}
						}

						GetBlobBoundary(redBlob);
						CColorStain *stainPtr = new CColorStain();
						stainList->push_back(stainPtr);
						stainPtr->m_RedRgnPtr = ptr;
						stainPtr->m_RedBlob = redBlob;
						(*hitData->GetBlobData(RED_COLOR))[i] = NULL;
						ptr = NULL;
						for (int j = 0; j < (int)blueBlobs.size(); j++)
						{
							stainPtr->m_BlueBlobs.push_back(blueBlobs[j]);
							blueBlobs[j] = NULL;
						}
						stainPtr->m_GreenBlob = new CBlobData(width, height);
						stainPtr->m_GreenBlob->CopyData(redBlob);
					}
					delete[] pixelCounts;
					FreeBlobList(&blueBlobs);
				}

				FreeBlobList(hitData->GetBlobData(BLUE_COLOR));
				if (ptr != NULL)
				{
					delete ptr;
					ptr = NULL;
				}
			}
		}
		FreeBlobList(hitData->GetBlobData(RED_COLOR));
	}
	delete[] image;
	delete[] map;
}

void CHitFindingOperation::GetColorStains(CCTCParams *params, CRGNData *hitData, vector<CColorStain *> *stainList)
{
	int width = hitData->GetWidth();
	int height = hitData->GetHeight();
	unsigned short *image = new unsigned short[width * height];
	unsigned short *map = new unsigned short[width * height];

	for (int i = 0; i < (int)hitData->GetBlobData(RED_COLOR)->size(); i++)
	{
		CBlobData *redBlob = (*hitData->GetBlobData(RED_COLOR))[i];
		if ((redBlob->m_PixelCount >= m_MinBlobPixelCount) &&
			(redBlob->m_PixelCount <= m_MaxBlobPixelCount))
		{
			int x0, y0;
			hitData->GetPosition(&x0, &y0);
			CRGNData *ptr = new CRGNData(x0, y0, hitData->GetWidth(), hitData->GetHeight());
			ptr->SetCPI(RED_COLOR, hitData->GetCPI(RED_COLOR));
			ptr->SetCPI(GREEN_COLOR, hitData->GetCPI(GREEN_COLOR));
			ptr->SetCPI(BLUE_COLOR, hitData->GetCPI(BLUE_COLOR));
			
			GetBlueBlobs(params, hitData, redBlob);
			vector<CBlobData *> blueBlobs;
			for (int j = 0; j < (int)(int)hitData->GetBlobData(BLUE_COLOR)->size(); j++)
			{
				CBlobData *blueBlob = (*hitData->GetBlobData(BLUE_COLOR))[j];
				int pixelCount = 0;
				GetIntersectPixels(redBlob, blueBlob, &pixelCount);
				if (pixelCount >= params->m_CTCParams[PARAM_INSIDEREDBLOB_BLUECOUNT])
				{
					blueBlobs.push_back(blueBlob);
					(*hitData->GetBlobData(BLUE_COLOR))[j] = NULL;
				}
			}
			if (blueBlobs.size() > 0)
			{
				unsigned char **ptrs = new unsigned char *[blueBlobs.size() + 1];
				ptrs[0] = redBlob->m_Map;
				for (int k = 0; k < (int)blueBlobs.size(); k++)
				{
					ptrs[k + 1] = blueBlobs[k]->m_Map;
				}
				memset(image, 0, sizeof(unsigned short) * width * height);
				memset(map, 0, sizeof(unsigned short) * width * height);
				for (int k=0; k<height; k++)
				{
					for (int h = 0; h < width; h++)
					{
						bool pixelOn = false;
						for (int l = 0; l <= (int)blueBlobs.size(); l++)
						{
							if ((!pixelOn) && (*ptrs[l] == (unsigned char)255))
								pixelOn = true;
							ptrs[l]++;
						}
						if (pixelOn)
							image[width * k + h] = (unsigned short)255;
					}
				}
				delete[] ptrs;
				unsigned short lastLabel = 0;
				m_Grouper.LabelConnectedComponents(image, width, height, 255, map, &lastLabel);
				int numLabels = (int)lastLabel;
				int *pixelCounts = new int[numLabels];
				memset(pixelCounts, 0, sizeof(int) * numLabels);
				for (int k = 0; k < height; k++)
				{
					for (int h = 0; h < width; h++)
					{
						int mapValue = (int)map[width * k + h];
						if ((mapValue > 0) && (mapValue <= numLabels))
						{
							pixelCounts[mapValue - 1]++;
						}
					}
				}

				int maxCount = 0;
				int maxLabel = 0;
				for (int k = 0; k < numLabels; k++)
				{
					if (pixelCounts[k] > maxCount)
					{
						maxCount = pixelCounts[k];
						maxLabel = k;
					}
				}

				if (maxCount > 0)
				{
					maxLabel++;
					memset(redBlob->m_Map, 0, sizeof(unsigned char) * width * height);
					redBlob->m_PixelCount = 0;
					redBlob->m_Rect.left = width;
					redBlob->m_Rect.right = 0;
					redBlob->m_Rect.top = height;
					redBlob->m_Rect.bottom = 0;
					redBlob->m_MaxIntensity = 0;
					redBlob->m_MaxIntenX = 0;
					redBlob->m_MaxIntenY = 0;
					unsigned short *imgPtr = hitData->GetImage(RED_COLOR);
					unsigned short *mapPtr = map;
					for (int k = 0; k < height; k++)
					{
						for (int h = 0; h < width; h++, mapPtr++, imgPtr++)
						{
							if (maxLabel == (int)*mapPtr)
							{
								redBlob->m_PixelCount++;
								redBlob->m_Map[width * k + h] = (unsigned char)255;
								if (h < redBlob->m_Rect.left)
									redBlob->m_Rect.left = h;
								if (h > redBlob->m_Rect.right)
									redBlob->m_Rect.right = h;
								if (k < redBlob->m_Rect.top)
									redBlob->m_Rect.top = k;
								if (k > redBlob->m_Rect.bottom)
									redBlob->m_Rect.bottom = k;
								if ((int)*imgPtr > redBlob->m_MaxIntensity)
								{
									redBlob->m_MaxIntensity = (int)*imgPtr;
									redBlob->m_MaxIntenX = h;
									redBlob->m_MaxIntenY = k;
								}
							}
						}
					}

					GetBlobBoundary(redBlob);
					CColorStain *stainPtr = new CColorStain();
					stainList->push_back(stainPtr);
					stainPtr->m_RedRgnPtr = ptr;
					stainPtr->m_RedBlob = redBlob;
					(*hitData->GetBlobData(RED_COLOR))[i] = NULL;
					ptr = NULL;
					for (int j = 0; j < (int)blueBlobs.size(); j++)
					{
						stainPtr->m_BlueBlobs.push_back(blueBlobs[j]);
						blueBlobs[j] = NULL;
					}
					stainPtr->m_GreenBlob = new CBlobData(width, height);
					stainPtr->m_GreenBlob->CopyData(redBlob);
				}

				delete[] pixelCounts;
			}
			FreeBlobList(&blueBlobs);
			FreeBlobList(hitData->GetBlobData(BLUE_COLOR));

			if (ptr != NULL)
			{
				delete ptr;
				ptr = NULL;
			}
		}
	}
	FreeBlobList(hitData->GetBlobData(RED_COLOR));
	delete[] image;
	delete[] map;
}

void CHitFindingOperation::GetPeaks(unsigned short *image, unsigned short *map, int width, int height, int numLabels, int minCount, int threshold, vector<PEAKINFO *> *peaks)
{
	int *pixelCounts = new int[numLabels];
	memset(pixelCounts, 0, sizeof(int) * numLabels);
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++)
		{
			int mapValue = (int)map[width * k + h];
			if ((mapValue > 0) && (mapValue <= numLabels))
			{
				pixelCounts[mapValue - 1]++;
			}
		}
	}

	int *blobIndex = new int[numLabels];
	memset(blobIndex, 0, sizeof(int) * numLabels);
	int count = 0;
	vector<PEAKINFO *> peakList;
	for (int i = 0; i < numLabels; i++)
	{
		if (pixelCounts[i] >= minCount)
		{
			count++;
			blobIndex[i] = count;
			PEAKINFO *peak = new PEAKINFO;
			memset(peak, 0, sizeof(PEAKINFO));
			peak->threshold = threshold;
			peakList.push_back(peak);
		}
	}

	unsigned short *imgPtr = image;
	unsigned short *mapPtr = map;
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++, imgPtr++)
		{
			if ((*mapPtr > 0) && (*mapPtr <= numLabels))
			{
				if (blobIndex[(int)*mapPtr - 1] > 0)
				{
					PEAKINFO *peak = peakList[blobIndex[(int)*mapPtr - 1] - 1];
					peak->pixelcount++;
					if ((int)*imgPtr > peak->maxIntensity)
					{
						peak->maxIntensity = (int)*imgPtr;
						peak->maxIntenX = h;
						peak->maxIntenY = k;
					}
				}
			}
		}
	}

	for (int i = 0; i < count; i++)
	{
		bool found = false;
		for (int j = 0; j < (int)peaks->size(); j++)
		{
			if ((peakList[i]->maxIntensity == (*peaks)[j]->maxIntensity)
				&& (peakList[i]->maxIntenX == (*peaks)[j]->maxIntenX)
				&& (peakList[i]->maxIntenY == (*peaks)[j]->maxIntenY))
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			peaks->push_back(peakList[i]);
		}
		else
		{
			delete peakList[i];
		}
		peakList[i] = NULL;
	}
	peakList.clear();
	delete[] pixelCounts;
	delete[] blobIndex;
}

void CHitFindingOperation::GetFrameMap(unsigned short *map, int width, int height, int numLabels)
{
	int *pixelCounts = new int[numLabels];
	memset(pixelCounts, 0, sizeof(int) * numLabels);
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++)
		{
			int mapValue = (int)map[width * k + h];
			if ((mapValue > 0) && (mapValue <= numLabels))
			{
				pixelCounts[mapValue - 1]++;
			}
		}
	}

	int *blobIndex = new int[numLabels];
	memset(blobIndex, 0, sizeof(int) * numLabels);
	int count = 0;
	for (int i = 0; i < numLabels; i++)
	{
		if (pixelCounts[i] >= DEFAULT_MIN_BLOBPIXELS)
		{
			count++;
			blobIndex[i] = count;
		}
	}

	unsigned short *mapPtr = map;
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++)
		{
			if ((*mapPtr > 0) && (*mapPtr <= numLabels))
			{
				if (blobIndex[(int)*mapPtr - 1] > 0)
				{
					*mapPtr = (unsigned short)0;
				}
				else
				{
					*mapPtr = (unsigned short)MAXUINT16;
				}
			}
			else
			{
				*mapPtr = (unsigned short)MAXUINT16;
			}
		}
	}

	delete[] pixelCounts;
	delete[] blobIndex;
}

void CHitFindingOperation::GetBlobDataFromPeak(unsigned short *image, unsigned short *map, int width, int height, PEAKINFO *peak, CBlobData *blob, 
	int threshold, vector<CBlobData *> *blobs, vector<PEAKINFO *> *peaks, int peakIndex)
{
	unsigned short lastLabel = 0;
	m_Grouper.LabelConnectedComponents(image, width, height, threshold, map, &lastLabel);
	int numLabels = (int)lastLabel;
	int *pixelCounts = new int[numLabels];
	memset(pixelCounts, 0, sizeof(int) * numLabels);
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++)
		{
			int mapValue = (int)map[width * k + h];
			if ((mapValue > 0) && (mapValue <= numLabels))
			{
				pixelCounts[mapValue - 1]++;
			}
		}
	}

	int *blobIndex = new int[numLabels];
	memset(blobIndex, 0, sizeof(int) * numLabels);
	vector<CBlobData *> blobList;
	int count = 0;
	for (int i = 0; i < numLabels; i++)
	{
		if (pixelCounts[i] >= DEFAULT_MIN_BLOBPIXELS)
		{
			count++;
			blobIndex[i] = count;
			CBlobData *newBlob = new CBlobData(width, height);
			newBlob->m_Threshold = threshold;
			newBlob->m_Rect.left = width;
			newBlob->m_Rect.top = height;
			blobList.push_back(newBlob);
		}
	}
	
	unsigned short *imgPtr = image;
	unsigned short *mapPtr = map;
	for (int k = 0; k < height; k++)
	{
		for (int h = 0; h < width; h++, mapPtr++, imgPtr++)
		{
			if ((*mapPtr > 0) && (*mapPtr <= numLabels))
			{
				if (blobIndex[(int)*mapPtr - 1] > 0)
				{
					CBlobData *newBlob = blobList[blobIndex[(int)*mapPtr - 1] - 1];
					newBlob->m_PixelCount++;
					newBlob->m_Map[width * k + h] = (unsigned char)255;
					if (h < newBlob->m_Rect.left)
						newBlob->m_Rect.left = h;
					if (h > newBlob->m_Rect.right)
						newBlob->m_Rect.right = h;
					if (k < newBlob->m_Rect.top)
						newBlob->m_Rect.top = k;
					if (k > newBlob->m_Rect.bottom)
						newBlob->m_Rect.bottom = k;
					if ((int)*imgPtr > newBlob->m_MaxIntensity)
					{
						newBlob->m_MaxIntensity = (int)*imgPtr;
						newBlob->m_MaxIntenX = h;
						newBlob->m_MaxIntenY = k;
					}
				}
			}
		}
	}

	bool found = false;
	for (int i = 0; i < (int)blobList.size(); i++)
	{
		if ((blobList[i]->m_MaxIntensity == peak->maxIntensity) &&
			(blobList[i]->m_MaxIntenX == peak->maxIntenX) &&
			(blobList[i]->m_MaxIntenY == peak->maxIntenY))
		{
			blob->CopyData(blobList[i]);
			found = true;
			break;
		}
	}

	if (!found)
	{
		int mapValue = (int)map[width * peak->maxIntenY + peak->maxIntenX];
		int index = blobIndex[mapValue - 1] - 1;
		for (int i = 0; i < peakIndex; i++)
		{
			if (((*peaks)[i]->maxIntensity == blobList[index]->m_MaxIntensity) &&
				((*peaks)[i]->maxIntenX == blobList[index]->m_MaxIntenX) &&
				((*peaks)[i]->maxIntenY == blobList[index]->m_MaxIntenY))
			{
				blob->CopyData(blobList[index]);
				found = true;
				break;
			}
		}
		if (found)
		{
			for (int i = 0; i < (int)blobs->size(); i++)
			{
				if (((*blobs)[i] != NULL) &&
					((*blobs)[i]->m_MaxIntensity == blob->m_MaxIntensity) &&
					((*blobs)[i]->m_MaxIntenX == blob->m_MaxIntenX) &&
					((*blobs)[i]->m_MaxIntenY == blob->m_MaxIntenY))
				{
					delete (*blobs)[i];
					(*blobs)[i] = NULL;
					break;
				}
			}
		}
	}
	FreeBlobList(&blobList);
	delete[] pixelCounts;
	delete[] blobIndex;
}


void CHitFindingOperation::SetPixelCountLimits(int minPixels, int maxPixels)
{
	m_MinBlobPixelCount = minPixels;
	m_MaxBlobPixelCount = maxPixels;
}

void CHitFindingOperation::FillCellScoreList(CRGNData *hitData, CListCtrl *cellScoreList, int hitIndex)
{
	cellScoreList->DeleteAllItems();
	CString labelStr;
	int itemIndex = 0;
	labelStr = _T("RgnIdx");
	int idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitIndex);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr = _T("RedAvg");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->m_RedValue);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr.Format(_T("%d"), hitData->m_RedScore);
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("CellSize");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%.1f"), hitData->m_CellSizeValue);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr.Format(_T("%d"), hitData->m_CellSizeScore);
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("N/C Ratio");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%.1f"), hitData->m_NCRatio);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr.Format(_T("%d"), hitData->m_NCScore);
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("WBC Gr= CPI+");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->m_WBCGreenAvg);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr = _T("Green Above WBC");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->m_GreenAboveWBC);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr.Format(_T("%d"), hitData->m_GreenScore);
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("GreenAvg =CPI+");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->m_GreenValue);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr = _T("%Brighter");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%.1f"), hitData->m_GreenRingValue);
	cellScoreList->SetItemText(idx, 1, labelStr);
	labelStr.Format(_T("%d"), hitData->m_GreenRingScore);
	cellScoreList->SetItemText(idx, 2, labelStr);
	labelStr = _T("Total");
	idx = cellScoreList->InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), hitData->GetScore());
	cellScoreList->SetItemText(idx, 2, labelStr);
}

void CHitFindingOperation::GetCenterCell(CRGNData *ptr, vector<CRGNData *> *cellList,
	int x, int y, CListCtrl *cellScoreList, int hitIndex)
{
	if (cellList->size() > 0)
	{
		FreeBlobList(ptr->GetBlobData(RED_COLOR));
		FreeBlobList(ptr->GetBlobData(GREEN_COLOR));
		FreeBlobList(ptr->GetBlobData(BLUE_COLOR));
		FreeBlobList(ptr->GetBlobData(RB_COLORS));
		FreeBlobList(ptr->GetBlobData(GB_COLORS));
	}
	if ((x != -1) && (y != -1))
	{
		int index = -1;
		for (int i = 0; i < (int)cellList->size(); i++)
		{
			int left, right, top, bottom;
			(*cellList)[i]->GetBoundingBox(&left, &top, &right, &bottom);
			if ((x >= left) && (x <= right) && (y >= top) && (y <= bottom))
			{
				index = i;
				break;
			}
		}

		if (index >= 0)
		{
			ptr->CopyRegionData((*cellList)[index]);
			ptr->m_HitIndex = hitIndex;
			if (cellScoreList != NULL)
				FillCellScoreList(ptr, cellScoreList, hitIndex);
		}
	}
	else
	{
		if (cellList->size() > 0)
		{
			int index = -1;
			int minDistance = MAXUINT16;
			int centerX = ptr->GetWidth() / 2;
			int centerY = ptr->GetHeight() / 2;
			for (int i = 0; i < (int)cellList->size(); i++)
			{
				int left, right, top, bottom;
				(*cellList)[i]->GetBoundingBox(&left, &top, &right, &bottom);
				int dx = centerX - (left + right) / 2;
				int dy = centerY - (top + bottom) / 2;
				int distance = dx * dx + dy * dy;
				if (distance < minDistance)
				{
					minDistance = distance;
					index = i;
				}
			}
			if (index > -1)
			{
				ptr->CopyRegionData((*cellList)[index]);
				ptr->m_HitIndex = hitIndex;
				if (cellScoreList != NULL)
					FillCellScoreList(ptr, cellScoreList, hitIndex);
			}
		}
	}
}

void CHitFindingOperation::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CHitFindingOperation::GetIntersectPixels(CBlobData *blob1, CBlobData *blob2, int *pixelCount)
{
	int count = 0;
	for (int i = 0; i < blob1->m_Height; i++)
	{
		for (int j = 0; j < blob1->m_Width; j++)
		{
			if ((blob2->m_Map[blob1->m_Width * i + j] == (unsigned char)255) &&
				(NeighborhoodON(blob1, i, j))) 
				count++;
		}
	}
	*pixelCount = count;
}

void CHitFindingOperation::ErosionOperation(unsigned char *inImage, unsigned char *outImage, int width, int height)
{
	memset(outImage, 0, sizeof(unsigned char) * width * height);

	for (int i = 1; i < (height - 1); i++)
	{
		for (int j = 1; j < (width - 1); j++)
		{
			bool keep = true;
			for (int k = -1; k < 2; k++)
			{
				for (int h = -1; h < 2; h++)
				{
					if (inImage[width * (i + k) + (j + h)] != (unsigned char)255)
					{
						keep = false;
						break;
					}
				}
				if (!keep)
					break;
			}
			if (keep)
				outImage[width * i + j] = (unsigned char)255;
		}
	}
}

void CHitFindingOperation::DilationOperation(unsigned char *inImage, unsigned char *outImage, int width, int height)
{
	memset(outImage, 0, sizeof(unsigned char) * width * height);

	for (int i = 1; i < (height - 1); i++)
	{
		for (int j = 1; j < (width - 1); j++)
		{
			bool keep = false;
			for (int k = -1; k < 2; k++)
			{
				for (int h = -1; h < 2; h++)
				{
					if (inImage[width * (i + k) + (j + h)] == (unsigned char)255)
					{
						keep = true;
						break;
					}
				}
				if (keep)
					break;
			}
			if (keep)
				outImage[width * i + j] = (unsigned char)255;
		}
	}
}

void CHitFindingOperation::GetBlobDataFromMap(CBlobData *blob, unsigned char *map, int label, unsigned short *image, int threshold)
{
	blob->m_Rect.left = blob->m_Width;
	blob->m_Rect.right = 0;
	blob->m_Rect.top = blob->m_Height;
	blob->m_Rect.bottom = 0;
	blob->m_MaxIntensity = 0;
	blob->m_MaxIntenX = 0;
	blob->m_MaxIntenY = 0;
	blob->m_PixelCount = 0;
	blob->m_Threshold = threshold;
	unsigned char *mapPtr = map;
	unsigned char *temp = new unsigned char[blob->m_Width * blob->m_Height];
	unsigned char *tempPtr = temp;
	unsigned char blobLabel = (unsigned char)label;
	unsigned short *imgPtr = image;
	for (int i = 0; i < blob->m_Height; i++)
	{
		for (int j = 0; j < blob->m_Width; j++, mapPtr++, tempPtr++, imgPtr++)
		{
			if (*mapPtr == blobLabel)
			{
				if (i < blob->m_Rect.top)
					blob->m_Rect.top = i;
				if (i > blob->m_Rect.bottom)
					blob->m_Rect.bottom = i;
				if (j < blob->m_Rect.left)
					blob->m_Rect.left = j;
				if (j > blob->m_Rect.right)
					blob->m_Rect.right = j;
				*tempPtr = (unsigned char)255;
				blob->m_PixelCount++;
				if (*imgPtr > blob->m_MaxIntensity)
				{
					blob->m_MaxIntensity = *imgPtr;
					blob->m_MaxIntenX = j;
					blob->m_MaxIntenY = i;
				}
			}
			else
				*tempPtr = (unsigned char)0;
		}
	}
	memcpy(blob->m_Map, temp, sizeof(unsigned char) * blob->m_Width * blob->m_Height);
	delete[] temp;
}

void CHitFindingOperation::GetBlobBoundary(CBlobData *blob)
{
	unsigned char *temp1 = new unsigned char[blob->m_Width * blob->m_Height];
	ErosionOperation(blob->m_Map, temp1, blob->m_Width, blob->m_Height);
	blob->m_Boundary->clear();
	for (int i = 0; i < blob->m_Height; i++)
	{
		for (int j = 0; j < blob->m_Width; j++)
		{
			if ((blob->m_Map[blob->m_Width * i + j] == (unsigned char)255) &&
				(temp1[blob->m_Width * i + j] == (unsigned char)0))
			{
				unsigned int pos = GetPos((unsigned short)j, (unsigned short)i);
				blob->m_Boundary->push_back(pos);
			}
		}
	}
	delete[] temp1;
}

void CHitFindingOperation::SaveImage(CString filename, unsigned short *map, int width, int height)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		CString textline;
		for (int i = 0; i < height; i++)
		{
			textline = _T("");
			for (int j = 0; j < width; j++)
			{
				CString textword;
				if (j < (width - 1))
					textword.Format(_T("%d,"), map[width * i + j]);
				else
					textword.Format(_T("%d\n"), map[width * i + j]);
				textline += textword;
			}
			theFile.WriteString(textline);
		}
		theFile.Close();
	}
}

void CHitFindingOperation::SaveMap(CString filename, unsigned char *map, int width, int height)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		CString textline;
		for (int i = 0; i < height; i++)
		{
			textline = _T("");
			for (int j = 0; j < width; j++)
			{
				CString textword;
				if (j < (width - 1))
					textword.Format(_T("%d,"), map[width * i + j]);
				else
					textword.Format(_T("%d\n"), map[width * i + j]);
				textline += textword;
			}
			theFile.WriteString(textline);
		}
		theFile.Close();
	}
}

unsigned int CHitFindingOperation::GetPos(unsigned short x, unsigned short y)
{
	unsigned int pos = (y & 0xFFFF) << 16;
	pos |= (x & 0xFFFF);

	return pos;
}

int CHitFindingOperation::GetX(unsigned int pos)
{
	return (int)(pos & 0xFFFF);
}

int CHitFindingOperation::GetY(unsigned int pos)
{
	return (int)((pos >> 16) & 0xFFFF);
}

bool CHitFindingOperation::NeighborhoodON(CBlobData *redBlob, int i, int j)
{
	if (((i - 1) >= 0) && ((j - 1) >= 0) && (redBlob->m_Map[redBlob->m_Width * (i - 1) + (j - 1)] == (unsigned char)255))
		return true;
	if (((i - 1) >= 0) && (redBlob->m_Map[redBlob->m_Width * (i - 1) + j] == (unsigned char)255))
		return true;
	if (((i - 1) >= 0) && ((j + 1) < redBlob->m_Width) && (redBlob->m_Map[redBlob->m_Width * (i - 1) + (j + 1)] == (unsigned char)255))
		return true;
	if (((j - 1) >= 0) && (redBlob->m_Map[redBlob->m_Width * i + (j - 1)] == (unsigned char)255))
		return true;
	if (redBlob->m_Map[redBlob->m_Width * i + j] == (unsigned char)255)
		return true;
	if (((j + 1) < redBlob->m_Width) && (redBlob->m_Map[redBlob->m_Width * i + (j + 1)] == (unsigned char)255))
		return true;
	if (((i + 1) < redBlob->m_Height) && ((j - 1) >= 0) && (redBlob->m_Map[redBlob->m_Width * (i + 1) + (j - 1)] == (unsigned char)255))
		return true;
	if (((i + 1) < redBlob->m_Height) && (redBlob->m_Map[redBlob->m_Width * (i + 1) + j] == (unsigned char)255))
		return true;
	if (((i + 1) < redBlob->m_Height) && ((j + 1) < redBlob->m_Width) && (redBlob->m_Map[redBlob->m_Width * (i + 1) + (j + 1)] == (unsigned char)255))
		return true;
	return false;
}

int CHitFindingOperation::GetAspectRatio(CBlobData *blob, CRGNData *ptr)
{
	int aspectRatio = 0;

	if (blob->m_Boundary->size() > 0)
	{
		int maxDX = 0;
		int maxDY = 0;
		int maxDistance = 0;
		int maxi = 0;
		int maxj = 0;
		int maxX0 = 0;
		int maxY0 = 0;
		int maxX1 = 0;
		int maxY1 = 0;
		for (int i = 0; i < (int)(blob->m_Boundary->size() - 1); i++)
		{
			unsigned int pos = (*blob->m_Boundary)[i];
			int x0 = GetX(pos);
			int y0 = GetY(pos);
			for (int j = i + 1; j < (int)blob->m_Boundary->size(); j++)
			{
				pos = (*blob->m_Boundary)[j];
				int x1 = GetX(pos);
				int y1 = GetY(pos);
				int dx = x1 - x0;
				int dy = y1 - y0;
				int distance = dx * dx + dy * dy;
				if (distance > maxDistance)
				{
					maxDistance = distance;
					maxDX = dx;
					maxDY = dy;
					maxi = i;
					maxj = j;
					maxX0 = x0;
					maxY0 = y0;
					maxX1 = x1;
					maxY1 = y1;
				}
			}
		}
		double maxDistance2 = sqrt(maxDistance);
		ptr->m_LongAxisLength = (int) maxDistance2;
		int maxDistance1 = 0;
		for (int i = 0; i < (int)blob->m_Boundary->size(); i++)
		{
			if ((i == maxi) || (i == maxj))
				continue;
			unsigned int pos = (*blob->m_Boundary)[i];
			int x0 = GetX(pos);
			int y0 = GetY(pos);
			int distance = (int) (abs(maxDY * x0 - maxDX * y0 + maxX1 * maxY0 - maxY1 * maxX0) / maxDistance2);
			if (distance > maxDistance1)
				maxDistance1 = distance;
		}
		maxDistance1 = 2 * maxDistance1;
		if (maxDistance1 > ptr->m_LongAxisLength)
		{
			maxDistance = maxDistance1;
			maxDistance1 = ptr->m_LongAxisLength;
			ptr->m_LongAxisLength = maxDistance;
		}
		aspectRatio = 100 * maxDistance1 / ptr->m_LongAxisLength;
	}
	
	return aspectRatio;
}

