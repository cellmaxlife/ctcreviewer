#include "stdafx.h"
#include "CTCParams.h"
#include "string.h"

CCTCParams::CCTCParams()
{
	m_CTCParams[(int)PARAM_RED_CUTOFF] = 20;
	m_CTCParams[(int)PARAM_GREEN_CUTOFF] = 20;
	m_CTCParams[(int)PARAM_BLUE_CUTOFF] = 0;
	m_CTCParams[(int)PARAM_RED_CONTRAST] = 760;
	m_CTCParams[(int)PARAM_GREEN_CONTRAST] = 900;
	m_CTCParams[(int)PARAM_BLUE_CONTRAST] = 1000;
	m_CTCParams[(int)PARAM_RED_THRESHOLD] = 180;
	m_CTCParams[(int)PARAM_GREEN_THRESHOLD] = 40;
	m_CTCParams[(int)PARAM_BLUE_THRESHOLD] = 150;
	m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT] = 900;
	m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT] = 100;
	m_CTCParams[(int)PARAM_AVERAGECK15] = 300;
	m_CTCParams[(int)PARAM_AVERAGECK10] = 220;
	m_CTCParams[(int)PARAM_AVERAGECK5] = 150;
	m_CTCParams[(int)PARAM_AVERAGECK_5] = 100;
	m_CTCParams[(int)PARAM_CELLSIZE15] = 22;
	m_CTCParams[(int)PARAM_CELLSIZE10] = 18;
	m_CTCParams[(int)PARAM_CELLSIZE5] = 15;
	m_CTCParams[(int)PARAM_CELLSIZE_5] = 11;
	m_CTCParams[(int)PARAM_CELLSIZE_10] = 9;
	m_CTCParams[(int)PARAM_NCRATIO0] = 100;
	m_CTCParams[(int)PARAM_NCRATIO15] = 90;
	m_CTCParams[(int)PARAM_NCRATIO10] = 80;
	m_CTCParams[(int)PARAM_NCRATIO5] = 70;
	m_CTCParams[(int)PARAM_CD45AVERAGE_25] = 30;
	m_CTCParams[(int)PARAM_CD45AVERAGE_20] = 25;
	m_CTCParams[(int)PARAM_CD45AVERAGE_15] = 20;
	m_CTCParams[(int)PARAM_CD45AVERAGE_10] = 15;
	m_CTCParams[(int)PARAM_CD45AVERAGE_5] = 10;
	m_CTCParams[(int)PARAM_CD45AVERAGE5] = -15;
	m_CTCParams[(int)PARAM_CD45AVERAGE10] = -30;
	m_CTCParams[(int)PARAM_CD45RING25] = 20;
	m_CTCParams[(int)PARAM_CD45RING20] = 15;
	m_CTCParams[(int)PARAM_CD45RING15] = 10;
	m_CTCParams[(int)PARAM_CD45RING10] = 5;
	m_CTCParams[(int)PARAM_CD45RING5] = 2;
	m_CTCParams[(int)PARAM_RINGPIXELS] = 3;
	m_CTCParams[(int)PARAM_BOUNDINGBOX_ASPECTRATIO] = 35;
	m_CTCParams[(int)PARAM_BOUNDINGBOX_PIXELCOUNTRATIO] = 35;
	m_CTCParams[(int)PARAM_REDTHRESHOLD_DROP] = 50;
	m_CTCParams[(int)PARAM_SATURATED_REDCOUNT] = 3000;
	m_CTCParams[(int)PARAM_SATURATED_BLUECOUNT] = 5000;
	m_CTCParams[(int)PARAM_INSIDEREDBLOB_BLUECOUNT] = 35;
	m_CTCParams[(int)PARAM_LASTONE] = 1;

	m_CTCParamNames[(int)PARAM_RED_CUTOFF] = _T("RedCutoff");
	m_CTCParamNames[(int)PARAM_GREEN_CUTOFF] = _T("GreenCutoff");
	m_CTCParamNames[(int)PARAM_BLUE_CUTOFF] = _T("BlueCutoff");;
	m_CTCParamNames[(int)PARAM_RED_CONTRAST] = _T("RedContrast");
	m_CTCParamNames[(int)PARAM_GREEN_CONTRAST] = _T("GreenContrast");
	m_CTCParamNames[(int)PARAM_BLUE_CONTRAST] = _T("BlueContrast");
	m_CTCParamNames[(int)PARAM_RED_THRESHOLD] = _T("RedBoundaryThreshold");
	m_CTCParamNames[(int)PARAM_GREEN_THRESHOLD] = _T("GreenBoundaryThreshold");
	m_CTCParamNames[(int)PARAM_BLUE_THRESHOLD] = _T("BlueBoundaryThreshold");
	m_CTCParamNames[(int)PARAM_MAX_BLOBPIXELCOUNT] = _T("MaxBlobPixelcount");
	m_CTCParamNames[(int)PARAM_MIN_BLOBPIXELCOUNT] = _T("MinBlobPixelcount");
	m_CTCParamNames[(int)PARAM_AVERAGECK15] = _T("AverageCK15");
	m_CTCParamNames[(int)PARAM_AVERAGECK10] = _T("AverageCK10");
	m_CTCParamNames[(int)PARAM_AVERAGECK5] = _T("AverageCK5");
	m_CTCParamNames[(int)PARAM_AVERAGECK_5] = _T("AverageCK-5");
	m_CTCParamNames[(int)PARAM_CELLSIZE15] = _T("CellSize15");
	m_CTCParamNames[(int)PARAM_CELLSIZE10] = _T("CellSize10");
	m_CTCParamNames[(int)PARAM_CELLSIZE5] = _T("CellSize5");
	m_CTCParamNames[(int)PARAM_CELLSIZE_5] = _T("CellSize-5");
	m_CTCParamNames[(int)PARAM_CELLSIZE_10] = _T("CellSize-10");
	m_CTCParamNames[(int)PARAM_NCRATIO0] = _T("NCRatio0");
	m_CTCParamNames[(int)PARAM_NCRATIO15] = _T("NCRatio15");;
	m_CTCParamNames[(int)PARAM_NCRATIO10] = _T("NCRatio10");
	m_CTCParamNames[(int)PARAM_NCRATIO5] = _T("NCRatio5");
	m_CTCParamNames[(int)PARAM_CD45AVERAGE_25] = _T("CD45Average-25");
	m_CTCParamNames[(int)PARAM_CD45AVERAGE_20] = _T("CD45Average-20");
	m_CTCParamNames[(int)PARAM_CD45AVERAGE_15] = _T("CD45Average-15");
	m_CTCParamNames[(int)PARAM_CD45AVERAGE_10] = _T("CD45Average-10");
	m_CTCParamNames[(int)PARAM_CD45AVERAGE_5] = _T("CD45Average-5");
	m_CTCParamNames[(int)PARAM_CD45AVERAGE5] = _T("CD45Average5");
	m_CTCParamNames[(int)PARAM_CD45AVERAGE10] = _T("CD45Average10");
	m_CTCParamNames[(int)PARAM_CD45RING25] = _T("CD45Ring25");
	m_CTCParamNames[(int)PARAM_CD45RING20] = _T("CD45Ring20");
	m_CTCParamNames[(int)PARAM_CD45RING15] = _T("CD45Ring15");
	m_CTCParamNames[(int)PARAM_CD45RING10] = _T("CD45Ring10");
	m_CTCParamNames[(int)PARAM_CD45RING5] = _T("CD45Ring5");
	m_CTCParamNames[(int)PARAM_RINGPIXELS] = _T("RingPixels");
	m_CTCParamNames[(int)PARAM_BOUNDINGBOX_ASPECTRATIO] = _T("BoundingBoxAspectRatio");
	m_CTCParamNames[(int)PARAM_BOUNDINGBOX_PIXELCOUNTRATIO] = _T("BoundingBoxPixelCountRatio");
	m_CTCParamNames[(int)PARAM_REDTHRESHOLD_DROP] = _T("RedThresholdDrop");
	m_CTCParamNames[(int)PARAM_SATURATED_REDCOUNT] = _T("SaturatedRedCount");
	m_CTCParamNames[(int)PARAM_SATURATED_BLUECOUNT] = _T("SaturatedBlueCount");
	m_CTCParamNames[(int)PARAM_INSIDEREDBLOB_BLUECOUNT] = _T("InsideRedBlobBlueCount");
	m_CTCParamNames[(int)PARAM_LASTONE] = _T("LastParameter");

	m_WBCGreenAvg = 550;
}

void CCTCParams::LoadCTCParams(CString filename)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			for (int i = 0; i < (((int)PARAM_LASTONE) + 1); i++)
			{
				int index = textline.Find(m_CTCParamNames[i]);
				if (index > -1)
				{
					CString name(m_CTCParamNames[i]);
					index += name.GetLength() + 1;
					m_CTCParams[i] = _wtoi(textline.Mid(index));
					break;
				}
			}
		}
		theFile.Close();
	}
}

void CCTCParams::SaveCTCParams(CString filename)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		CString textline;
		for (int i = 0; i < (((int)PARAM_LASTONE) + 1); i++)
		{
			textline.Format(_T("%s=%d\n"), m_CTCParamNames[i], m_CTCParams[i]);
			theFile.WriteString(textline);
		}
		theFile.Close();
	}
}